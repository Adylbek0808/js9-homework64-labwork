import React from 'react';
import { Button, Card } from 'react-bootstrap';
import moment from "moment";

const PostMin = props => {    
    return (
        <Card className="mb-3">
            <Card.Header style={{ color: 'grey' }}>Created on ({moment(props.time).format('MMMM Do YYYY, HH:mm:ss ')})</Card.Header>
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                {props.children}
                <Button variant="primary" onClick={props.btnPress}>{props.btnName} </Button>
            </Card.Body>
        </Card>
    );
};

export default PostMin;