import React from 'react';
import { Button, Container, Form } from 'react-bootstrap';

const EditPost = props => {
    return (
        <Container style={{ padding: "10px" }}>
            <h1>{props.title}</h1>
            <Form onSubmit={props.postHandler}>
                <Form.Group>
                    <Form.Label>Title: </Form.Label>
                    <Form.Control type="text" name="title" onChange={props.postInfoChanged} defaultValue={props.postInfo.title} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control as="textarea" name='text' rows={6} onChange={props.postInfoChanged} defaultValue={props.postInfo.text}/>
                </Form.Group>
                <Button variant="primary" type="submit" >{props.btnType}</Button>
                {props.children}
            </Form>
        </Container>
    );
};

export default EditPost;