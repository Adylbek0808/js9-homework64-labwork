import React from 'react';
import { Nav, Navbar } from "react-bootstrap";

const NavbarTop = props => {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">My Blog</Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse >
                <Nav className="ml-auto">
                    <Nav.Link onClick={() => props.flipPage('')}>Home</Nav.Link>
                    <Nav.Link onClick={() => props.flipPage('add')}>Add</Nav.Link>
                    <Nav.Link onClick={() => props.flipPage('about')}>About</Nav.Link>
                    <Nav.Link onClick={() => props.flipPage('contacts')}>Contacts</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default NavbarTop;