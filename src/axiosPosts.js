import axios from 'axios';

const axiosPosts = axios.create({
    baseURL: 'https://js9-burgeradylbek-default-rtdb.firebaseio.com/labwork64/'
});

export default axiosPosts;