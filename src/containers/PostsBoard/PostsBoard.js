import React from 'react';
import { Container } from 'react-bootstrap';
import PostMin from '../../components/PostMin/PostMin';

const PostsBoard = ({ posts, showPost }) => {

    const postsList = posts.map(post => (
        <PostMin
            key={post.id}
            title={post.title}
            time={post.time}
            id={post.id}
            btnPress={() => showPost(post.id)}
            btnName = "Read More"
        />
    ))

    return (
        <Container style={{ padding: "10px" }}>
            {postsList}
        </Container>
    );
};

export default PostsBoard;