import React, {useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import axiosPosts from '../../axiosPosts';
import NavbarTop from '../../components/NavbarTop/NavbarTop';
import PostsBoard from '../PostsBoard/PostsBoard';
import AddPostContainer from '../AddPostContainer/AddPostContainer';
import FullPostContainer from '../FullPostContainer/FullPostContainer';
import AboutPage from '../AboutPage/AboutPage';
import ContactsPage from '../ContactsPage/ContactsPage';

const MainPage = props => {

    const [posts, setPosts] = useState([]);

    const flipPage = link => {
        props.history.push({
            pathname: '/' + link
        });       
    };

    const onePostDetail = link => {
        flipPage('posts/' + link);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axiosPosts.get('/posts.json')
                const test2 = Object.keys(response.data).map(item => {
                    const id = item;
                    const post = {
                        ...response.data[item],
                        id: id
                    };
                    return post
                });
                setPosts(test2);
            } catch (error) {
                console.error("Network error")
            };
        };
        if (props.history.location.pathname === "/") {
            fetchData().catch(console.error)
            console.log("Show when home")
        };
    }, [props.history.location.pathname]);

    return (
        <Container >
            <NavbarTop flipPage={flipPage} />
            <Switch>
                <Route
                    path='/' exact
                    render={props => (
                        <PostsBoard
                            {...props}
                            posts={posts}
                            showPost={onePostDetail}
                        />
                    )}
                />
                <Route
                    path='/add'
                    render={props => (
                        <AddPostContainer
                            {...props}
                            returnToMainPage={flipPage}
                        />
                    )}
                />
                <Route
                    path='/posts/:id'
                    render={props => (
                        <FullPostContainer
                            {...props}
                            returnToMainPage={flipPage}
                        />
                    )}
                />
                <Route
                    path='/about'
                    render={props => (
                        <AboutPage
                            {...props}
                            returnToMainPage={flipPage}
                        />
                    )}
                />
                <Route
                    path='/contacts'
                    render={props => (
                        <ContactsPage
                            {...props}
                            returnToMainPage={flipPage}
                        />
                    )}
                />
            </Switch>




        </Container>


    );
};

export default MainPage;