import React, { useEffect, useState } from 'react';
import { Button, Card, Container } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import axiosPosts from '../../axiosPosts';
import EditPost from '../../components/EditPost/EditPost';
import PostMin from '../../components/PostMin/PostMin';

const FullPostContainer = props => {
    const [post, setPost] = useState({});

    useEffect(() => {
        const getPostData = async () => {
            try {
                const response = await axiosPosts.get('/posts/' + props.match.params.id + '.json');
                const postData = response.data;
                setPost(postData);
            } catch (e) {
                console.error("Network error");
            };
        };
        getPostData().catch(console.error)
    }, [props.match.params.id]);

    const editPostDetail = () => {
        props.history.replace({
            pathname: '/posts/' + props.match.params.id + '/edit'
        });
    };

    const postInfoChanged = event => {
        const {name, value} = event.target;
        setPost(prevState => ({
            ...post,
            [name]: value
        }));
    };

    const editPostHandler = async event => {
        event.preventDefault();
        const today = new Date();
        const message = {
            ...post,
            time: today.toISOString()
        };
        try {
            await axiosPosts.put('/posts/' + props.match.params.id + '.json', message)
        } finally {
            props.returnToMainPage('');
        };
        console.log(message);
    };
    const deletePostHandler = async event => {
        event.preventDefault();
        try {
            await axiosPosts.delete('/posts/' + props.match.params.id + '.json')
        } finally {
            props.returnToMainPage('');
        }
        console.log(props.match.params.id, "post deleted");
    };

    return (
        <Container className="p-4">
            <Switch>
                <Route
                    path={props.match.path} exact
                    render={props => (
                        <PostMin
                            time={post.time}
                            title={post.title}
                            btnPress={editPostDetail}
                            btnName="Edit this Post"
                            {...props}
                        >
                            <Card.Text>{post.text}</Card.Text>
                        </PostMin>
                    )}
                />
                <Route
                    path={props.match.path + '/edit'} exact
                    render={props => (
                        <EditPost
                            {...props} btnType="Save changes" title="Edit Post"
                            postInfo={post}
                            postHandler={editPostHandler}
                            postInfoChanged={postInfoChanged}
                        >
                            <Button variant="danger" className="ml-3" onClick={deletePostHandler}>Delete post</Button>
                        </EditPost>
                    )}
                />
            </Switch >
        </Container>
    );
};

export default FullPostContainer;