import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Form } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import axiosPosts from '../../axiosPosts';

const ContactsPage = props => {
    const [contacts, setContacts] = useState({});

    useEffect(() => {
        const getContactsData = async () => {
            try {
                const response = await axiosPosts.get('/contacts.json');
                const data = response.data
                setContacts(data)
            } catch (e) {
                console.error("Network error");
            }
        };
        if (props.history.location.pathname === '/contacts') {
            getContactsData().catch(console.error)
        };
    }, [props.history.location.pathname]);

    const sendEditedInfo = async event => {
        event.preventDefault();
        const editedInfo = {
            tel: document.getElementById('tel').value,
            email: document.getElementById('email').value,
            address: document.getElementById('address').value
        };
        try {
            await axiosPosts.put('/contacts.json', editedInfo)
        } finally {
            props.returnToMainPage('');
        };
        console.log(editedInfo);
        props.history.push({
            pathname: '/contacts'
        });
    };
    
    const editDetail = () => {
        props.history.replace({
            pathname: props.match.path + '/edit'
        });
    };

    return (
        <Container className='p-4'>
            <Switch>
                <Route
                    path={props.match.path} exact
                    render={props => (
                        <Card>
                            <Card.Header as="h5">Contacts Information</Card.Header>
                            <Card.Body>
                                <Card.Title>Telephone: {contacts.tel}</Card.Title>
                                <Card.Title>Email: {contacts.email}</Card.Title>
                                <Card.Title>Address: {contacts.address}</Card.Title>
                                <Button variant="primary"  onClick={editDetail}>Edit Contacts Information</Button>
                            </Card.Body>
                        </Card>
                    )}
                />
                <Route
                    path={props.match.path + '/edit'} exact
                    render={props => (
                        <Form onSubmit={sendEditedInfo}>
                            <Form.Group>
                                <Form.Label>New phone number: </Form.Label>
                                <Form.Control type="text" id="tel" defaultValue={contacts.tel} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>New Email</Form.Label>
                                <Form.Control type="email" id='email' rows={6} defaultValue={contacts.email} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>New Address</Form.Label>
                                <Form.Control type="text" id='address' rows={6} defaultValue={contacts.address} />
                            </Form.Group>
                            <Button variant="primary" type="submit" >Save Changes</Button>
                        </Form>
                    )}
                />
            </Switch>
        </Container>
    );
};

export default ContactsPage;