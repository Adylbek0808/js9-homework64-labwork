import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Form } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import axiosPosts from '../../axiosPosts';

const AboutPage = props => {

    const [aboutInfo, setAboutInfo] = useState({});

    useEffect(() => {
        const getPersonalData = async () => {
            try {
                const response = await axiosPosts.get('/about.json');
                const data = response.data;
                setAboutInfo(data)
            } catch (e) {
                console.error("Network error");
            };
        };
        if (props.history.location.pathname === "/about") {
            getPersonalData().catch(console.error)
        };
    }, [props.history.location.pathname]);


    const editDetail = () => {
        props.history.replace({
            pathname: props.match.path + '/edit'
        });
    };

    const sendEditedInfo = async event => {
        event.preventDefault();
        const editedInfo = {
            name: document.getElementById('name').value,
            info: document.getElementById('info').value
        };
        try {
            await axiosPosts.put('/about.json', editedInfo)
        } finally {
            props.returnToMainPage('');
        };
        console.log(editedInfo);
        props.history.push({
            pathname: '/about'
        });
    };

    return (
        <Container className='p-4'>
            <Switch>
                <Route
                    path={props.match.path} exact
                    render={props => (
                        <Card>
                            <Card.Header as="h5">About Me</Card.Header>
                            <Card.Body>
                                <Card.Title>My name is: {aboutInfo.name}</Card.Title>

                                <Card.Text>
                                    {aboutInfo.info}
                                </Card.Text>
                                <Button variant="primary" onClick={editDetail} >Edit Personal Info</Button>
                            </Card.Body>
                        </Card>
                    )}
                />
                <Route
                    path={props.match.path + '/edit'} exact
                    render={props => (
                        <Form onSubmit={sendEditedInfo}>
                            <Form.Group>
                                <Form.Label>Edit my Name: </Form.Label>
                                <Form.Control type="text" id="name" defaultValue={aboutInfo.name} />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>My personal information</Form.Label>
                                <Form.Control as="textarea" id='info' rows={6} defaultValue={aboutInfo.info} />
                            </Form.Group>
                            <Button variant="primary" type="submit" >Save Changes</Button>
                        </Form>
                    )}
                />
            </Switch >
        </Container>
    );
};

export default AboutPage;