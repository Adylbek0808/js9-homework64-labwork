import React, { useState } from 'react';
import axiosPosts from '../../axiosPosts';
import EditPost from '../../components/EditPost/EditPost';

const AddPostContainer = props => {

    const [post, setPost] = useState({});

    const postInfoChanged = event => {
        const {name, value} = event.target;
        setPost(prevState => ({
            ...post,
            [name]: value
        }));
    };

    const postHandler = async event => {
        event.preventDefault();
        const today = new Date();
        const message = {
            ...post,
            time: today.toISOString()
        };
        try {
            await axiosPosts.post('/posts.json', message)
        } finally {
            props.returnToMainPage('');
        };
        console.log(message);
    };
    return (
        <EditPost
            {...props} btnType="Add post" title="Add New Post"
            postHandler={postHandler}
            postInfoChanged={postInfoChanged}
            postInfo={post}
        />
    );
};

export default AddPostContainer;